package bo.com.joselo.app.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {
    final static Logger LOG = Logger.getLogger(ConfigProperties.class);
    private static Properties properties;

    private static Properties getConfigPropertiesFile() {
        if (properties == null) {
            Properties properties = new Properties();
            try {
                String filename = "properties" + File.separator + "configuration.properties";
                File file = new File(filename);
                InputStream fileInput;
                if (file.exists()) {
                    fileInput = new FileInputStream(file);
                } else {
                    fileInput = (ConfigProperties.class).getClassLoader().getResourceAsStream("configuration.properties");
                }

                properties.load(fileInput);
                fileInput.close();
                return properties;

            } catch (IOException ex) {
                LOG.error(ex.getMessage());
                ex.printStackTrace();
                return null;
            }
        }

        return properties;
    }

    public static String getDatabaseDriver() {
        return getConfigPropertiesFile().getProperty("database.driver");
    }

    public static String getDatabaseUrl() {
        return getConfigPropertiesFile().getProperty("database.url");
    }

    public static String getDatabaseUser() {
        return getConfigPropertiesFile().getProperty("database.user");
    }

    public static String getDatabasePassword() {
        return getConfigPropertiesFile().getProperty("database.password");
    }
}
