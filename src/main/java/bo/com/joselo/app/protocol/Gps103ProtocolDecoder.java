
package bo.com.joselo.app.protocol;

import bo.com.joselo.app.dao.DeviceDao;
import bo.com.joselo.app.dao.PlotDao;
import bo.com.joselo.app.model.ExtendedInfoFormatter;
import bo.com.joselo.app.model.Plot;
import bo.com.joselo.app.model.Position;
import bo.com.joselo.app.service.BaseProtocolDecoder;
import bo.com.joselo.app.service.ServerManager;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Gps103ProtocolDecoder extends BaseProtocolDecoder {
    private final static Logger log = Logger.getLogger(Gps103ProtocolDecoder.class);

    private DeviceDao deviceDao = new DeviceDao();
    private PlotDao plotDao = new PlotDao();

    public Gps103ProtocolDecoder(ServerManager serverManager) {
        super(serverManager);
    }

    static private Pattern pattern = Pattern.compile(
            "imei:" +
                    "(\\d+)," +                         // IMEI
                    "([^,]+)," +                        // Alarm
                    "(\\d{2})/?(\\d{2})/?(\\d{2})\\s?" + // Local Date
                    "(\\d{2}):?(\\d{2})(?:\\d{2})?," +  // Local Time
                    "[^,]*," +
                    "[FL]," +                           // F - full / L - low
                    "(\\d{2})(\\d{2})(\\d{2})\\.(\\d{3})," + // Time UTC (HHMMSS.SSS)
                    "([AV])," +                         // Validity
                    "(\\d+)(\\d{2}\\.\\d+)," +          // Latitude (DDMM.MMMM)
                    "([NS])," +
                    "(\\d+)(\\d{2}\\.\\d+)," +          // Longitude (DDDMM.MMMM)
                    "([EW])?," +
                    "(\\d+\\.?\\d*)," +                 // Speed
                    "(\\d+\\.?\\d*)?(?:," +             // Course
                    "(\\d+\\.?\\d*)?)?" +               // Altitude
                    ".*");

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        String sentence = (String) msg;
        log.info("decode gps103 message: " + sentence);

        // Send response #1
        if (sentence.contains("##")) {
            if (channel != null) {
                log.info("write LOAD");
                channel.write("LOAD", channel.getLocalAddress());
            }
            return null;
        }

        // Send response #2
        if (sentence.length() == 15 && Character.isDigit(sentence.charAt(0))) {
            if (channel != null) {
                log.info("write ON");
                channel.write("ON", channel.getLocalAddress());
            }
            return null;
        }

        // Parse message
        Matcher parser = pattern.matcher(sentence);
        if (!parser.matches()) {
            return null;
        }

        // Create new position
        Position position = new Position();
        ExtendedInfoFormatter extendedInfo = new ExtendedInfoFormatter("gps103");
        Integer index = 1;

        // Get device by IMEI
        String imei = parser.group(index++);
        try {
            position.setDeviceId(deviceDao.getIdByImei(imei));
        } catch (Exception error) {
            log.warn("Unknown device - " + imei);
            return null;
        }

        // Alarm message
        String alarm = parser.group(index++);
        position.setCommand(alarm);
        extendedInfo.set("alarm", alarm);

        // Date
        Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        time.clear();
        time.set(Calendar.YEAR, 2000 + Integer.valueOf(parser.group(index++)));
        time.set(Calendar.MONTH, Integer.valueOf(parser.group(index++)) - 1);
        time.set(Calendar.DAY_OF_MONTH, Integer.valueOf(parser.group(index++)));

        int localHours = Integer.valueOf(parser.group(index++));
        int localMinutes = Integer.valueOf(parser.group(index++));

        int utcHours = Integer.valueOf(parser.group(index++));
        int utcMinutes = Integer.valueOf(parser.group(index++));

        // Time
        time.set(Calendar.HOUR, localHours);
        time.set(Calendar.MINUTE, localMinutes);
        time.set(Calendar.SECOND, Integer.valueOf(parser.group(index++)));
        time.set(Calendar.MILLISECOND, Integer.valueOf(parser.group(index++)));

        // Timezone calculation
        int deltaMinutes = (localHours - utcHours) * 60 + localMinutes - utcMinutes;
        if (deltaMinutes <= -12 * 60) {
            deltaMinutes += 24 * 60;
        } else if (deltaMinutes > 12 * 60) {
            deltaMinutes -= 24 * 60;
        }
        time.add(Calendar.MINUTE, -deltaMinutes);
        position.setTime(time.getTime());

        // Validity
        position.setValid(parser.group(index++).compareTo("A") == 0 ? true : false);
        position.setAccuracy(0.0);

        // Latitude
        Double latitude = Double.valueOf(parser.group(index++));
        latitude += Double.valueOf(parser.group(index++)) / 60;
        if (parser.group(index++).compareTo("S") == 0) latitude = -latitude;
        position.setLatitude(latitude);

        // Longitude
        Double longitude = Double.valueOf(parser.group(index++));
        longitude += Double.valueOf(parser.group(index++)) / 60;
        String hemisphere = parser.group(index++);
        if (hemisphere != null) {
            if (hemisphere.compareTo("W") == 0) longitude = -longitude;
        }
        position.setLongitude(longitude);

        // Speed
        position.setSpeed(Double.valueOf(parser.group(index++)));

        // Course
        String course = parser.group(index++);
        if (course != null) {
            position.setCourse(Double.valueOf(course));
        } else {
            position.setCourse(0.0);
        }

        // Altitude
        String altitude = parser.group(index++);
        if (altitude != null) {
            position.setAltitude(Double.valueOf(altitude));
        } else {
            position.setAltitude(0.0);
        }

        // Extended info
        position.setExtendedInfo(extendedInfo.toString());
        position.setSentDate(new Date());
        storePlot(sentence, channel);
        return position;
    }

    private void storePlot(String sentence, Channel channel) throws SQLException {
        Plot plot = new Plot();
        plot.setCommand(sentence);
        plot.setPort(((InetSocketAddress) channel.getLocalAddress()).getPort());
        plot.setSentDate(new java.sql.Timestamp(new Date().getTime()));
        // save on db
        plotDao.createPlot(plot);
    }
}
