package bo.com.joselo.app;

import bo.com.joselo.app.service.ServerManager;
import bo.com.joselo.app.util.ConfigProperties;
import org.apache.log4j.Logger;

import java.util.Locale;

public class Main {
    private final static Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        Locale.setDefault(Locale.getDefault());

        Class.forName(ConfigProperties.getDatabaseDriver());

        final ServerManager service = new ServerManager();
        service.init();

        LOG.info("Starting server...");

        service.start();

        // Shutdown server properly
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                LOG.info("Shutting down server...");
                service.stop();
            }
        });
    }
}