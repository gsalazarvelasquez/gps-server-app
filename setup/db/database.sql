DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `color` varchar(255) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `mail_drop_alarm` varchar(255) DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_device_imei` (`imei`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `positions_buffer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions_buffer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `altitude` double DEFAULT NULL,
  `battery` varchar(255) DEFAULT NULL,
  `command` varchar(500) DEFAULT NULL,
  `course` double DEFAULT NULL,
  `extended_info` text,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  `speed` double DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `valid` bit(1) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_position_buffer_device_id` (`device_id`),
  CONSTRAINT `fk_position_buffer_device_id` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3820 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `altitude` double DEFAULT NULL,
  `battery` varchar(255) DEFAULT NULL,
  `command` varchar(500) DEFAULT NULL,
  `course` double DEFAULT NULL,
  `extended_info` text,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  `speed` double DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `valid` bit(1) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_position_device_id` (`device_id`),
  CONSTRAINT `fk_position_device_id` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4599 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `plots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plots` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `port` bigint(20) DEFAULT NULL,
  `command` varchar(500) DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34810 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `alarm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `battery` int(11) DEFAULT NULL,
  `date_received` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `is_sent` bit(1) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `speed` double DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;