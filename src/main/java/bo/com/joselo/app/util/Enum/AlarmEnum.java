package bo.com.joselo.app.util.Enum;

public enum AlarmEnum {
    DROP_OFF("DROP"),
    LOW_BATTERY("GTBPL"),
    CONNECT_POWER_SUPPLY("GTMPN"),
    DESCONNECT_POWER_SUPPLY("GTMPF"),
    SOS("GTSOS");

    private String label;

    AlarmEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
