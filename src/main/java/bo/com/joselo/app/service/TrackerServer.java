package bo.com.joselo.app.service;


import org.jboss.netty.bootstrap.Bootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;

import java.net.InetSocketAddress;

/**
 * Tracker server
 */
public abstract class TrackerServer {

    private final ServerManager serverManager;
    private final Bootstrap bootstrap;

    public TrackerServer(ServerManager serverManager, Bootstrap bootstrap, Integer port) {
        this.serverManager = serverManager;
        this.bootstrap = bootstrap;

        // Set appropriate channel factory
        bootstrap.setFactory(GlobalChannelFactory.getFactory());

        this.port = port;

        bootstrap.setPipelineFactory(new BasePipelineFactory(serverManager, this) {
            @Override
            protected void addSpecificHandlers(ChannelPipeline pipeline) {
                TrackerServer.this.addSpecificHandlers(pipeline);
            }
        });
    }

    protected abstract void addSpecificHandlers(ChannelPipeline pipeline);

    /**
     * Server port
     */
    private Integer port;

    /**
     * Opened channels
     */
    private final ChannelGroup allChannels = new DefaultChannelGroup();

    public ChannelGroup getChannelGroup() {
        return allChannels;
    }

    /**
     * Start server
     */
    public void start() {
        InetSocketAddress endpoint = new InetSocketAddress(port);

        Channel channel = ((ServerBootstrap) bootstrap).bind(endpoint);
        if (channel != null) {
            getChannelGroup().add(channel);
        }
    }

    /**
     * Stop server
     */
    public void stop() {
        ChannelGroupFuture future = getChannelGroup().close();
        future.awaitUninterruptibly();
    }

}
