# GPS Server App
Servidor encargado de: 
- Recepcionar las posiciones
- Interpretar los tramas de los dispositivos GPS
- Guardar las posicines en la base de datos.


## Requerimientos para iniciar el proyecto

- [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/) 
- [Maven 3](https://maven.apache.org/download.cgi) 