package bo.com.joselo.app.dao;

import bo.com.joselo.app.model.Alarm;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.Date;


public class AlarmDao {
    private final static Logger LOG = Logger.getLogger(AlarmDao.class);

    public Long add(Alarm alarm) throws SQLException {
        LOG.info("store Alarm: " + alarm.toString());


        String query_ = "INSERT INTO gps_alarm (" +
                "type, " +
                "is_sent, " +
                "device_id, " +
                "date_received, " +
                "date_sent, " +
                "latitude, " +
                "longitude, " +
                "speed, " +
                "battery) " + "VALUE ('"
                + alarm.getType() + "',"
                + alarm.getSent() + ",'"
                + alarm.getDeviceId() + "', '"
                + new java.sql.Timestamp(new Date().getTime()) + "', "
                + alarm.getDateSent() + ", '"
                + alarm.getLatitude() + "', '"
                + alarm.getLongitude() + "', '"
                + alarm.getSpeed() + "', '"
                + alarm.getBattery() + "');";

        return DataDaoManager.executeQuery(query_);
    }
}
