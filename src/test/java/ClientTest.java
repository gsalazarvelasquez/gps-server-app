import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

public class ClientTest {
    private final static Logger LOG = Logger.getLogger(ClientTest.class);

    public static void main(String args[]) {
        try {
            String cmdDLUX103 = "imei:359710041258675,tracker,1501262326,77355967,F,152631.000,A,1747.2183,S,06312.7528,W,0.00,0;";
            String msgtk102 = "imei:864180031484095,tracker,1501262326,77355967,F,152631.000,A,1747.2183,S,06312.7528,W,0.00,0;";

            final String HOST = "localhost"; // AWS IP Server
            final int PORT = 5000;
            InetAddress address = InetAddress.getByName(HOST);
            Socket socket = new Socket(address, PORT);

            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            LOG.info("Message: " + msgtk102);
            bw.write(msgtk102);
            bw.flush();
            socket.close();
            LOG.info("Message Sent!!!");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}