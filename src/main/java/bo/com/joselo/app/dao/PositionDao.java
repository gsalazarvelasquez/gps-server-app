package bo.com.joselo.app.dao;

import bo.com.joselo.app.model.Position;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class PositionDao {
    private final static Logger log = Logger.getLogger(PositionDao.class);

    public Long createPosition(Position position) throws Exception {
        log.info("createPosition: " + position);

        String query = "INSERT INTO gps_positions (device_id, " +
                "time" +
                ", valid" +
                ", accuracy" +
                ", altitude" +
                ", latitude" +
                ", longitude" +
                ", speed" +
                ", course" +
                ", extended_info" +
                ", command" +
                ", battery" +
                ", sent_date)" +
                " VALUES (" + position.getDeviceId() +
                ", '" + new java.sql.Timestamp(position.getTime().getTime()) + "'" +
                ", " + position.getValid() +
                ", " + position.getAccuracy() +
                ", " + position.getAltitude() +
                ", " + position.getLatitude() +
                ", " + position.getLongitude() +
                ", " + position.getSpeed() +
                ", " + position.getCourse() +
                ", '" + position.getExtendedInfo() + "'" +
                ", '" + (position.getCommand() == null ? "" : position.getCommand()) + "'" +
                ", '" + (position.getBattery() == null ? "" : position.getBattery()) + "'" +
                ", '" + new java.sql.Timestamp(position.getSentDate().getTime()) + "')";

        return DataDaoManager.executeQuery(query);
    }


    public static Position getLastPositionByDeviceId(Long deviceId) throws Exception {
        log.info("getLastPositionByDeviceId: " + deviceId);

        String query = "SELECT * FROM gps_positions WHERE device_id = ? ORDER BY id DESC LIMIT 1";

        return getLastPositionBy(query, deviceId);
    }

    public static Position getLastPositionBy(String query, Long deviceId) throws Exception {
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        Position position = null;
        try {
            dbConnection = DataDaoManager.getDataSource().getConnection();
            preparedStatement = dbConnection.prepareStatement(query);
            preparedStatement.setLong(1, deviceId);
            // execute select SQL statement
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                position = getPositionFromResultSet(rs);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();

            if (dbConnection != null)
                dbConnection.close();
        }

        return position;
    }

    public Long createPosition(List<Position> positionList) throws Exception {
        Long id = 0L;
        log.info("LIST OF POSITIONS, SORTED BY TIME");
        for (Position position : positionList) {
            log.info("position: " + position.getTime());
            id = createPosition(position);
        }

        log.info("END SORTED POSITIONS");
        return id;
    }

    public static Position getPositionFromResultSet(ResultSet rs) throws SQLException {
        Position position = new Position();
        position.setDeviceId(rs.getLong("device_id"));
        position.setTime(rs.getTimestamp("time"));
        position.setValid(rs.getBoolean("valid"));
        position.setAccuracy(rs.getDouble("accuracy"));
        position.setAltitude(rs.getDouble("altitude"));
        position.setLatitude(rs.getDouble("latitude"));
        position.setLongitude(rs.getDouble("longitude"));
        position.setSpeed(rs.getDouble("speed"));
        position.setCourse(rs.getDouble("course"));
        position.setExtendedInfo(rs.getString("extended_info"));
        position.setCommand(rs.getString("command"));
        position.setBattery(rs.getString("battery"));
        position.setSentDate(rs.getTimestamp("sent_date"));
        return position;
    }
}
