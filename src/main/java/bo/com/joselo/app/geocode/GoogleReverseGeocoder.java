
package bo.com.joselo.app.geocode;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class GoogleReverseGeocoder implements ReverseGeocoder {
    private final static Logger log = Logger.getLogger(GoogleReverseGeocoder.class);

    private final static String MARKER = "\"formatted_address\" : \"";

    @Override
    public String getAddress(double latitude, double longitude) {

        try {
            URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=false");
            URLConnection connection = url.openConnection();

            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

            // Find address line
            String line;
            while ((line = reader.readLine()) != null) {
                int index = line.indexOf(MARKER);
                if (index != -1) {
                    return line.substring(index + MARKER.length(), line.length() - 2);
                }
            }

            reader.close();

        } catch (Exception error) {
            log.warn(error);
        }

        return null;
    }

}
