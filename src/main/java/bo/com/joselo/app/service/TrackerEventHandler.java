package bo.com.joselo.app.service;

import bo.com.joselo.app.dao.PositionDao;
import bo.com.joselo.app.model.Position;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;

import java.util.List;

/**
 * Tracker message handler
 */
@ChannelHandler.Sharable
public class TrackerEventHandler extends IdleStateAwareChannelHandler {
    private final static Logger log = Logger.getLogger(TrackerEventHandler.class);

    private Long processSinglePosition(Position position) {
        if (position == null) {
            log.warn("processSinglePosition null message");
            return 0L;
        }
        // Write position to database
        Long id = null;
        PositionDao positionDao = new PositionDao();

        try {
            id = positionDao.createPosition(position);

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return id;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        log.info("messageReceived: " + e.getMessage());
        if (e.getMessage() instanceof Position) {
            processSinglePosition((Position) e.getMessage());
        } else if (e.getMessage() instanceof List) {
            List<Position> positions = (List<Position>) e.getMessage();
            positions.forEach(this::processSinglePosition);
        }
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
        log.info("Closing connection by disconnect");
        e.getChannel().close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        log.info("Closing connection by exception");
        e.getChannel().close();
    }

    @Override
    public void channelIdle(ChannelHandlerContext ctx, IdleStateEvent e) {
        log.info("Closing connection by timeout");
        e.getChannel().close();
    }
}