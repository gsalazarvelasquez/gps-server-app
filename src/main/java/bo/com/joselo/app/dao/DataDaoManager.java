package bo.com.joselo.app.dao;

import bo.com.joselo.app.util.ConfigProperties;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class DataDaoManager {
    private final static Logger LOG = Logger.getLogger(DataManager.class);
    private static DataSource dataSource;

    public static DataSource getDataSource() {
        if (dataSource == null)
            dataSource = getMySQLDataSource();
        return dataSource;
    }

    public static DataSource getMySQLDataSource() {
        MysqlDataSource mysqlDS = null;
        try {
            mysqlDS = new MysqlDataSource();
            mysqlDS.setURL(ConfigProperties.getDatabaseUrl());
            mysqlDS.setUser(ConfigProperties.getDatabaseUser());
            mysqlDS.setPassword(ConfigProperties.getDatabasePassword());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return mysqlDS;
    }

    public static Long executeQuery(String query) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        long result = 0;
        try {
            dbConnection = DataDaoManager.getDataSource().getConnection();
            statement = dbConnection.createStatement();
            result = statement.executeUpdate(query);

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);

        } finally {
            if (statement != null)
                statement.close();

            if (dbConnection != null)
                dbConnection.close();
        }
        return result;
    }
}
