package bo.com.joselo.app.model;

import java.util.Date;


public class Position extends Data {

    private Date time;
    private Boolean valid;
    private Double accuracy;
    private Double altitude;
    private Double latitude;
    private Double longitude;
    private Double speed;
    private Double course;
    private String command;
    private Long deviceId;
    private String battery;
    private String extendedInfo;
    private Date sentDate;

    public static Position createNew() {
        Position position = new Position();
        position.time = new Date();
        position.valid = true;
        position.accuracy = 0.0;
        position.speed = 0.0;
        position.course = 0.0;
        position.command = "";
        position.deviceId = 0L;
        position.battery = "0.0";
        position.extendedInfo = "";
        position.sentDate = new Date();
        return position;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getCourse() {
        return course;
    }

    public void setCourse(Double course) {
        this.course = course;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getExtendedInfo() {
        return extendedInfo;
    }

    public void setExtendedInfo(String extendedInfo) {
        this.extendedInfo = extendedInfo;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "Position{" +
                "time=" + time +
                ", valid=" + valid +
                ", accuracy=" + accuracy +
                ", altitude=" + altitude +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", speed=" + speed +
                ", course=" + course +
                ", command='" + command + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", battery='" + battery + '\'' +
                ", extendedInfo='" + extendedInfo + '\'' +
                ", sentDate='" + sentDate + '\'' +
                '}';
    }
}
