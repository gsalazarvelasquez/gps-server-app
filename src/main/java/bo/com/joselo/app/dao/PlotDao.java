package bo.com.joselo.app.dao;

import bo.com.joselo.app.model.Plot;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.Date;


public class PlotDao {
    private final static Logger LOG = Logger.getLogger(PlotDao.class);

    public Long createPlot(Plot plot) throws SQLException {
        LOG.info("Create plot: " + plot);

        String query = "INSERT INTO gps_plots (" +
                "port, " +
                "sent_date, " +
                "command) " +
                "VALUES ("
                + plot.getPort()
                + ", '" + plot.getSentDate()
                + "', '" + plot.getCommand() + "');";

        return DataDaoManager.executeQuery(query);
    }
}
