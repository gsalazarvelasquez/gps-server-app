package bo.com.joselo.app.util.Enum;

public enum WondexEventEnum {

    GET_LOCATION(0, "GETLOCATION"),
    REC(1, "REC"),
    TRACK(2, "TRACK"),
    SPD(3, "SPD"),
    EMSMS(4, "EMSMS"),
    PSM_OFF(34, "PSM_OFF"),
    PSM_ON(37, "PSM_ON");

    private final int eventId;
    private final String eventName;

    WondexEventEnum(int eventId, String eventName) {
        this.eventId = eventId;
        this.eventName = eventName;
    }

    public int getEventId() {
        return eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public static String findEventNameById(int id) {
        for (WondexEventEnum e : values())
            if (e.eventId == id)
                return e.getEventName();
        return GET_LOCATION.getEventName();
    }
}