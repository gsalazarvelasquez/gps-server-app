package bo.com.joselo.app.model;

import java.util.Date;

public class Alarm {

    private String type;
    private Date dateReceived;
    private Date dateSent;
    private Boolean isSent;
    private Long deviceId;
    private Double latitude;
    private Double longitude;
    private Double speed;
    private Integer battery;

    public static Alarm createFromPosition(Position position) {
        Alarm alarm = new Alarm();
        alarm.type = position.getCommand();
        alarm.deviceId = position.getDeviceId();
        alarm.isSent = false;
        alarm.dateReceived = new Date();
        alarm.dateSent = null;
        alarm.latitude = position.getLatitude();
        alarm.longitude = position.getLongitude();
        alarm.speed = position.getSpeed();
        final Double battery = position.getBattery() == null || position.getBattery().isEmpty() ? 0.0 : Double.parseDouble(position.getBattery());
        alarm.battery = battery.intValue();
        return alarm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Boolean sent) {
        isSent = sent;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "type='" + type + '\'' +
                ", dateReceived=" + dateReceived +
                ", dateSent=" + dateSent +
                ", isSent=" + isSent +
                ", deviceId=" + deviceId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", speed=" + speed +
                ", battery=" + battery +
                '}';
    }
}
