
package bo.com.joselo.app.service;

import bo.com.joselo.app.dao.DataManager;
import bo.com.joselo.app.dao.DatabaseDataManager;
import bo.com.joselo.app.geocode.GoogleReverseGeocoder;
import bo.com.joselo.app.geocode.ReverseGeocoder;
import bo.com.joselo.app.protocol.Gps103ProtocolDecoder;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.jboss.netty.handler.codec.string.StringDecoder;
import org.jboss.netty.handler.codec.string.StringEncoder;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class ServerManager {

    private final List<TrackerServer> serverList = new LinkedList<TrackerServer>();

    public void addTrackerServer(TrackerServer trackerServer) {
        serverList.add(trackerServer);
    }

    private DataManager dataManager;

    private ReverseGeocoder reverseGeocoder;

    public ReverseGeocoder getReverseGeocoder() {
        return reverseGeocoder;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    private Properties properties;

    public Properties getProperties() {
        return properties;
    }

    public void init() throws Exception {
        // Load properties
        properties = new Properties();

        dataManager = new DatabaseDataManager(properties);

        initGeocoder();
        initGps103Server(5000);
    }

    public void start() {
        for (Object server : serverList) {
            ((TrackerServer) server).start();
        }
    }

    public void stop() {
        for (Object server : serverList) {
            ((TrackerServer) server).stop();
        }
        // Release resources
        GlobalChannelFactory.release();
        GlobalTimer.release();

    }

    public void destroy() {
        serverList.clear();
    }

    private void initGeocoder() throws IOException {
        reverseGeocoder = new GoogleReverseGeocoder();
    }

    private void initGps103Server(final Integer protocol) throws SQLException {
        serverList.add(new TrackerServer(this, new ServerBootstrap(), protocol) {
            @Override
            protected void addSpecificHandlers(ChannelPipeline pipeline) {
                byte delimiter1[] = {(byte) '\r', (byte) '\n'};
                byte delimiter2[] = {(byte) '\n'};
                byte delimiter3[] = {(byte) ';'};
                pipeline.addLast("frameDecoder", new DelimiterBasedFrameDecoder(1024,
                        ChannelBuffers.wrappedBuffer(delimiter1),
                        ChannelBuffers.wrappedBuffer(delimiter2),
                        ChannelBuffers.wrappedBuffer(delimiter3)));
                pipeline.addLast("stringDecoder", new StringDecoder());
                pipeline.addLast("stringEncoder", new StringEncoder());
                pipeline.addLast("objectDecoder", new Gps103ProtocolDecoder(ServerManager.this));
            }
        });
    }
}
