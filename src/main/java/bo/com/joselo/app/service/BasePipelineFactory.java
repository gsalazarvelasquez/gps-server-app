package bo.com.joselo.app.service;

import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.logging.LoggingHandler;
import org.jboss.netty.handler.timeout.IdleStateHandler;

import java.net.InetSocketAddress;


public abstract class BasePipelineFactory implements ChannelPipelineFactory {
    private final static Logger LOG = Logger.getLogger(BasePipelineFactory.class);

    private final TrackerServer server;
    private Integer resetDelay;

    /**
     * Open channel handler
     */
    protected class OpenChannelHandler extends SimpleChannelHandler {

        private final TrackerServer server;

        public OpenChannelHandler(TrackerServer server) {
            this.server = server;
        }

        @Override
        public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) {
            server.getChannelGroup().add(e.getChannel());
        }
    }

    /**
     * Logging using global logger
     */
    protected class StandardLoggingHandler extends LoggingHandler {

        @Override
        public void log(ChannelEvent e) {

            if (e instanceof MessageEvent) {
                long initialTime = System.currentTimeMillis();

                MessageEvent event = (MessageEvent) e;
                StringBuilder msg = new StringBuilder();

                msg.append("[").append(((InetSocketAddress) e.getChannel().getLocalAddress()).getPort());
                msg.append((e instanceof DownstreamMessageEvent) ? " -> " : " <- ");

                msg.append(((InetSocketAddress) event.getRemoteAddress()).getAddress().getHostAddress()).append("]");

                // Append hex message
                if (event.getMessage() instanceof ChannelBuffer) {
                    msg.append(" - HEX: ");
                    msg.append(ChannelBuffers.hexDump((ChannelBuffer) event.getMessage()));

                    String hex = ChannelBuffers.hexDump((ChannelBuffer) event.getMessage());
                    msg.append((ChannelBuffer) event.getMessage());
                }

                long finalTime = System.currentTimeMillis();
                LOG.info(String.format("Message processed [%s]", (finalTime - initialTime)));

            } else if (e instanceof ExceptionEvent) {
                ExceptionEvent event = (ExceptionEvent) e;
                LOG.warn(event.getCause().toString());
            }
        }

    }

    public BasePipelineFactory(ServerManager serverManager, TrackerServer server) {
        this.server = server;
    }

    protected abstract void addSpecificHandlers(ChannelPipeline pipeline);

    @Override
    public ChannelPipeline getPipeline() {
        ChannelPipeline pipeline = Channels.pipeline();
        if (resetDelay != null) {
            pipeline.addLast("idleHandler", new IdleStateHandler(GlobalTimer.getTimer(), resetDelay, 0, 0));
        }
        pipeline.addLast("openHandler", new OpenChannelHandler(server));
        pipeline.addLast("logger", new StandardLoggingHandler());

        addSpecificHandlers(pipeline);

        pipeline.addLast("handler", new TrackerEventHandler());
        return pipeline;
    }

}
