package bo.com.joselo.app.dao;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DeviceDao {
    private final static Logger LOG = Logger.getLogger(DeviceDao.class);

    public Long getIdByImei(String imei) throws SQLException {
        Long deviceId = 0L;

        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT id FROM gps_device WHERE imei = " + imei;

        try {
            dbConnection = DataDaoManager.getDataSource().getConnection();
            statement = dbConnection.createStatement();
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                deviceId = rs.getLong("id");
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);

        } finally {
            if (statement != null)
                statement.close();

            if (dbConnection != null)
                dbConnection.close();
        }
        return deviceId;
    }

    public Long getDeviceTypeIdByDeviceId(Long deviceId) throws SQLException {
        Long deviceTypeId = 0L;
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT device_type_id FROM gps_device WHERE id = " + deviceId;

        try {
            dbConnection = DataDaoManager.getDataSource().getConnection();
            statement = dbConnection.createStatement();
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                deviceTypeId = rs.getLong("device_type_id");
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);

        } finally {
            if (statement != null)
                statement.close();

            if (dbConnection != null)
                dbConnection.close();
        }
        return deviceTypeId;
    }

    public String getImeiByDeviceId(Long id) throws SQLException {
        String imei = "";

        String selectTableSQL = "SELECT imei FROM device WHERE id = " + id;

        try (Connection dbConnection = DataDaoManager.getDataSource().getConnection();
             Statement statement = dbConnection.createStatement()) {
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                imei = rs.getString("imei");
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return imei;
    }
}
