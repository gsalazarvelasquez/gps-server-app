package bo.com.joselo.app.util;

import bo.com.joselo.app.dao.DeviceDao;

public class ProtocolUtil {

    public static String getValue(String[] data, int index) {
        return data[index];
    }

    public static Long deviceId(String imei) {
        try {
            DeviceDao deviceDao = new DeviceDao();
            return deviceDao.getIdByImei(imei);
        } catch (Exception error) {
            System.out.println("error: " + error.getMessage());
            return null;
        }
    }

    public static String getTypeCommand(String strCmd) {
        String typeCmd = "";
        String[] splitHead = strCmd.split(":");
        typeCmd = splitHead[1];
        return typeCmd;
    }


}
