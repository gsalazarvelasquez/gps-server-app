package bo.com.joselo.app.util;

public class UnitsConverter {

    public static double knotsFromKph(double value) { // km/h
        return value * 0.539957;
    }

    public static double knotsFromMph(double value) {
        return value * 0.868976;
    }

    public static double knotsFromMps(double value) { // m/s
        return value * 1.94384;
    }

    public static double knotsFromCps(double value) { // cm/s
        return value * 0.0194384449;
    }

}