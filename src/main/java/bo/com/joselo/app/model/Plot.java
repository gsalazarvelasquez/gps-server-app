package bo.com.joselo.app.model;

import java.io.Serializable;
import java.util.Date;

public class Plot implements Serializable {

    private Integer port;
    private Date sentDate;
    private String command;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "plot{" +
                "port=" + port +
                ", sentDate=" + sentDate +
                ", command='" + command + '\'' +
                '}';
    }
}
