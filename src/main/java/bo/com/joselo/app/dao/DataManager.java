package bo.com.joselo.app.dao;

import bo.com.joselo.app.model.Device;
import bo.com.joselo.app.model.Position;

import java.util.List;

/**
 * Data manager
 */
public interface DataManager {

    /**
     * Manage devices
     */
    public List<Device> getDevices() throws Exception;
    public Device getDeviceByImei(String imei) throws Exception;

    /**
     * Manage positions
     */
    public Long addPosition(Position position) throws Exception;
    public void updateLatestPosition(Long deviceId, Long positionId) throws Exception;

}
